from django.urls import path
from .views import list, create, update, delete

urlpatterns = [
    path('list/', list, name='setor_listar'),
    path('create/', create, name='setor_criar'),
    path('update/<int:id>/', update , name='setor_update'),
    path('delete/<int:id>/', delete , name='setor_delete'),
]
