from django.db import models

class Setor(models.Model):
    nome = models.CharField(max_length=200, null=False, blank=False, verbose_name='Nome')
    sigla = models.CharField(max_length=10, null=False, blank=False, verbose_name='Sigla')
    telefone = models.CharField(max_length=16, null=False, blank=False, verbose_name='Telefone')

    class Meta:
        verbose_name = 'Setor'
        verbose_name_plural = 'Setores'

    def __str__(self):
        return self.nome
