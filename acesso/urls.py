from django.urls import path
from .views import criar, atualizar, excluir, registar_saida


urlpatterns = [
    path('criar/<int:id>/', criar, name='acesso_criar'),
    path('atualizar/<int:id>/', atualizar, name='acesso_atualizar'),
    path('excluir/<int:id>/', excluir, name='acesso_excluir'),
    path('registar-saida/<int:id>/', registar_saida, name='acesso_registrar_saida')

]
