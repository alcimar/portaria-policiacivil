from django import forms

from acesso.models import Acesso


class AcessoFormulario(forms.ModelForm):
    class Meta:
        model = Acesso
        fields = ['setor']


class AcessoFormularioCompleto(forms.ModelForm):
    class Meta:
        model = Acesso
        fields = '__all__'
