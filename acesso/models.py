from django.db import models

from setor.models import Setor
from visitante.models import Visitante


class Acesso(models.Model):
    data_entrada = models.DateTimeField(null=False, blank=False, verbose_name='Data de entrada')
    data_saida = models.DateTimeField(null=True, blank=True, verbose_name='Data de saída')
    setor = models.ForeignKey(Setor, null=False, blank=False, verbose_name='Setor', on_delete=models.DO_NOTHING)
    visitante = models.ForeignKey(Visitante, blank=False, null=False, verbose_name='Visitante', on_delete=models.CASCADE)
    saiu = models.BooleanField(blank=True, null=True, default=False, verbose_name='Não saiu')

    class Meta:
        verbose_name = 'Acesso'
        verbose_name_plural = 'Acessos'
