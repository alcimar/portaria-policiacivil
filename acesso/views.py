from datetime import datetime
from django.utils import timezone

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.urls import reverse

from acesso.models import Acesso
from acesso.forms import AcessoFormulario, AcessoFormularioCompleto
from visitante.models import Visitante


@login_required(login_url='login')
def listar(request, id):

    visitante = get_object_or_404(Visitante, pk=id)
    
    acessos = visitante.acesso_set.all().order_by('-id')

    paginator = Paginator(acessos, 5)
    page = request.GET.get('page')
    acessos = paginator.get_page(page)

    variaveis = {
        'visitante': visitante,
        'acessos': acessos
    }

    return render(request, 'visitante/detalhe.html', variaveis)


@login_required(login_url='login')
def criar(request, id):
    form = AcessoFormulario(request.POST or None)
    visitante = get_object_or_404(Visitante, pk=id)
    acessos = visitante.acesso_set.all().order_by('-id')

    data_atual = timezone.now()

    if form.is_valid():
        form_temp = form.save(commit=False)
        form_temp.visitante = visitante
        form_temp.data_entrada = data_atual
        form_temp.save()
        messages.success(request, "Entrada registrada com sucesso!")
        return HttpResponseRedirect(reverse('visitante_detalhar', kwargs={'id': id}))


    variaveis = {
        'form': form,
        'acessos': acessos,
        'visitante': visitante,
        'visitante_id': id
    }
    return render(request, 'acesso/acesso_criar.html', variaveis)


@login_required(login_url='login')
def atualizar(request, id):
    acesso = get_object_or_404(Acesso, pk=id)
    visitante_id = acesso.visitante.id
    visitante = get_object_or_404(Visitante, pk=visitante_id)
    form = AcessoFormulario(request.POST or None, instance=acesso)

    if form.is_valid():
        print('entrou no if')
        form.save()
        messages.success(request, "Acesso atualizado com sucesso!")
        return HttpResponseRedirect(reverse('visitante_detalhar', kwargs={'id': visitante_id}))

    acessos = visitante.acesso_set.all().order_by('-id')

    context = {
        'acessos': acessos,
        'form': form,
        'visitante_id': visitante_id
    }

    return render(request, 'acesso/acesso_atualizar.html', context)


@login_required(login_url='login')
def excluir(request, id):
    acesso = get_object_or_404(Acesso, pk=id)
    visitante_id = acesso.visitante.id

    if request.method == 'POST':
        acesso.delete()
        messages.success(request, "Acesso excluído com sucesso!")
        return HttpResponseRedirect(reverse('visitante_detalhar', kwargs={'id': visitante_id}))

    context = {
        'visitante_id': visitante_id
    }
    return render(request, 'acesso/acesso_deletar.html', context)


def registar_saida(request, id):
    acesso = get_object_or_404(Acesso, pk=id)
    visitante_id = acesso.visitante.id
    data_atual = datetime.now()
    form = AcessoFormularioCompleto(instance=acesso)

    if request.method == 'POST':
        form_temp = form.save(commit=False)
        form_temp.data_saida = data_atual
        form_temp.saiu = True
        form_temp.save()
        messages.success(request, "Saída confirmada com sucesso!")

    return HttpResponseRedirect(reverse('visitante_detalhar', kwargs={'id': visitante_id}))
