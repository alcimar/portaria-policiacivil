from django.db import models
from pessoa.models import Pessoa


class Visitante(Pessoa):
    imagem = models.ImageField(upload_to='IMAGENS', null=False, blank=False, verbose_name='Imagem')

    class Meta:
        verbose_name = 'Visitante'
        verbose_name_plural = 'Visitantes'

