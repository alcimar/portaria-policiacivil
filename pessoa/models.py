from django.db import models


SEXO_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Feminino'),
)


class Pessoa(models.Model):
    nome = models.CharField(max_length=200, null=False, blank=False, verbose_name='Nome')
    cpf = models.CharField(max_length=14, null=False, blank=False, verbose_name='CPF')
    rg = models.CharField(max_length=14, null=False, blank=False, verbose_name='RG')
    telefone = models.CharField(max_length=16, null=False, blank=False, verbose_name='Telefone')
    sexo = models.CharField(max_length=10, choices=SEXO_CHOICES, blank=True, null=True, verbose_name='Sexo')
    data_nascimento = models.DateField(null=True, blank=True, verbose_name='Data de nascimento')

    def __str__(self):
        return 'Nome: %s - CPF: %s' % (self.nome, self.cpf)

    class Meta:
        abstract = True
